﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerBIOpenClaimMetrics
{
    class Program
    {
        static string sCON = "server=198.143.98.122;database=veritas;Min Pool Size=5;Max Pool Size=10000;Connect Timeout=60;User Id=sa;Password=NCC1701E";
        static string SQL;
        static DateTime CurDate;

        static void Main(string[] args)
        {
            DateTime Date1 = DateTime.Today;
            DateTime Date2 = new DateTime(2021, 8, 2);
            int iDaysDiff = ((TimeSpan)(Date1 - Date2)).Days;
            double cnt = 0;
            ClearTable();

            do
            {
                CurDate = Date2.AddDays(cnt);
                CalcTotalClaims();
                CalcOnlineClaims();
                CalcPhoneClaims();
                CalcOnlineClaimsAN();
                CalcPhoneClaimsAN();
                cnt++;
            } while (cnt < iDaysDiff);
        }

        static void CalcPhoneClaimsAN()
        {
            DBO.clsDBO clR = new DBO.clsDBO();
            SQL = "insert into Veritaspowerbi.dbo.openclaimmetrics " +
            "(ClaimOpenDate, ClaimType, Cnt) " +
            "select '" + CurDate + "', 'Phone Claims AN', count(*) from claim c " +
            "where c.CreDate >= '" + CurDate + "' " +
            "and c.CreDate < '" + CurDate.AddDays(1) + "' " +
            "and c.contractid in (select contractid from contract where contractno like '%an%') " +
            "and not c.claimno in (select ClaimNo from VeritasClaimsAN.dbo.Claim where not claimno is null) ";
            clR.RunSQL(SQL, sCON);
        }

        static void CalcOnlineClaimsAN()
        {
            DBO.clsDBO clR = new DBO.clsDBO();
            SQL = "insert into Veritaspowerbi.dbo.openclaimmetrics " +
            "(ClaimOpenDate, ClaimType, Cnt) " +
            "select '" + CurDate + "', 'Online Claims AN', count(*) from claim c " +
            "inner join VeritasClaimsAN.dbo.claim cl on cl.ClaimNo = c.ClaimNo " +
            "where c.CreDate >= '" + CurDate + "' " +
            "and c.CreDate < '" + CurDate.AddDays(1) + "' " +
            "and c.contractid in (select contractid from contract where contractno like '%an%') ";
            clR.RunSQL(SQL, sCON);
        }

        static void CalcPhoneClaims()
        {
            DBO.clsDBO clR = new DBO.clsDBO();
            SQL = "insert into Veritaspowerbi.dbo.openclaimmetrics " +
            "(ClaimOpenDate, ClaimType, Cnt) " +
            "select '" + CurDate + "', 'Phone Claims', count(*) from claim c " +
            "where c.CreDate >= '" + CurDate + "' " +
            "and c.CreDate < '" + CurDate.AddDays(1) + "' " +
            "and c.contractid in (select contractid from contract where not contractno like '%an%') " +
            "and not c.claimno in (select ClaimNo from VeritasClaims.dbo.Claim where not claimno is null) ";
            clR.RunSQL(SQL, sCON);
        }

        static void CalcOnlineClaims()
        {
            DBO.clsDBO clR = new DBO.clsDBO();
            SQL = "insert into Veritaspowerbi.dbo.openclaimmetrics " +
            "(ClaimOpenDate, ClaimType, Cnt) " +
            "select '" + CurDate + "', 'Online Claims', count(*) from claim c " +
            "inner join VeritasClaims.dbo.claim cl on cl.ClaimNo = c.ClaimNo " +
            "where c.CreDate >= '" + CurDate + "' " +
            "and c.CreDate < '" + CurDate.AddDays(1) + "' " +
            "and c.contractid in (select contractid from contract where not contractno like '%an%') ";
            clR.RunSQL(SQL, sCON);
        }


        static void CalcTotalClaims()
        {
            DBO.clsDBO clR = new DBO.clsDBO();
            SQL = "insert into Veritaspowerbi.dbo.openclaimmetrics " +
            "(ClaimOpenDate, ClaimType, Cnt) " +
            "select '" + CurDate + "', 'Total Claims', count(*) from claim " +
            "where CreDate >= '" + CurDate + "' " +
            "and CreDate < '" + CurDate.AddDays(1) + "' ";
            clR.RunSQL(SQL, sCON);

        }

        static void ClearTable()
        {
            DBO.clsDBO clR = new DBO.clsDBO();
            SQL = "truncate table Veritaspowerbi.dbo.openclaimmetrics ";
            clR.RunSQL(SQL, sCON);
        }
    }
}
